<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<form action="" method="post">
    <button class="btn btn-primary" type="submit" name="ten">نمایش حقوق کمتر 10 میلیون</button>
    <button class="btn btn-primary" type="submit" name="unit">نمایش کارمندان و واحد اداریشان</button>
    <button class="btn btn-primary" type="submit" name="salary">میانگین حقوق کارمندان هر واحد</button>
    <button class="btn btn-primary" type="submit" name="esf">نام واحد های اداری شعبه اصفهان</button>
    <button class="btn btn-primary" type="submit" name="shop">نام شعب به همراه تعداد واحد هر کدام</button>
    <button class="btn btn-primary mt-2" type="submit" name="emp">نام کارمندان به همراه نام شعبه</button>
    <button class="btn btn-primary mt-2" type="submit" name="esfAvg">میانگین حقوق کارمندان اصفهان</button>
    <button class="btn btn-primary mt-2" type="submit" name="shopNum">نام شعبه به همراه تعداد کارمند هر کدام</button>
    <button class="btn btn-primary mt-2" type="submit" name="esfW"> نام واحد های اداری شعبه اصفهان و تعداد کارمند ها</button>
    <button class="btn btn-primary mt-2" type="submit" name="unit2"> نام شعباتی که تعداد کارمندان آنها کمتر از 4 نفر است</button>
</form>
<?php
$host = 'localhost';
$user = 'root';
$password = '';
$dbname = 'shop';
$dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
$pdo = new PDO($dsn, $user, $password);

if (isset($_POST["ten"])) {
    $query = 'SELECT Name FROM shop.employee WHERE salary<10000000';
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $item) {
        echo $item['Name'] . "<br>";
    }
} elseif (isset($_POST["unit"])) {
    $query = 'SELECT employee.Name,unit.name FROM shop.employee INNER JOIN unit WHERE employee.Unit = unit.unit_code';
    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $item) {
        echo $item['name'] . '<=' . $item['Name'] . "<br>";
    }
} elseif (isset($_POST["salary"])) {
    $num = 8;

    for ($i = 1; $i <= $num; $i++) {
        $query = "SELECT AVG(Salary) FROM employee
WHERE Unit = $i";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($result as $item) {
            echo "unit $i =>" . floatval($item['AVG(Salary)']) . "<br>";
        }
    }
} elseif (isset($_POST["esf"])) {

    $query = "SELECT name FROM unit
WHERE shop_code = 4";

    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $item) {
        echo " واحد " . $item['name'] . "<br>";
    }
} elseif (isset($_POST["shop"])) {
    for ($i = 1; $i <= 4; $i++) {

        $query = "SELECT main_shop.city,COUNT(unit.unit_code) FROM main_shop
INNER JOIN unit
on main_shop.shop_code = unit.shop_code
WHERE main_shop.shop_code = $i";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        echo $result[0]["COUNT(unit.unit_code)"] . ' ' . $result[0]['city'] . " " . "<br>";

    }
} elseif (isset($_POST["emp"])) {


    $query = "SELECT employee.Name,main_shop.city FROM employee
    INNER JOIN unit
ON employee.Unit = unit.unit_code
INNER JOIN main_shop
ON unit.shop_code = main_shop.shop_code
    ";


    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo "<pre>";
    print_r($result);
    echo "<pre>";


} elseif (isset($_POST["esfAvg"])) {


    $query = "SELECT AVG(employee.Salary) FROM employee
  WHERE employee.Unit = 7 OR employee.Unit = 8 ";

    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo floatval($result[0]["AVG(employee.Salary)"]);

} elseif (isset($_POST["shopNum"])) {


    for ($i = 1; $i <= 4; $i++) {

        $query = "SELECT main_shop.city,COUNT(employee.Name) FROM main_shop
INNER JOIN unit
on main_shop.shop_code = unit.shop_code
INNER JOIN employee
ON employee.Unit = unit.unit_code
WHERE main_shop.shop_code = $i";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $result[0]["number of workers"] = $result[0]["COUNT(employee.Name)"];
        unset($result[0]["COUNT(employee.Name)"]);

        print_r($result);
        echo "<br>";

    }
}

elseif (isset($_POST["esfW"])) {

    $query = "SELECT COUNT(unit.unit_code), unit.name
FROM unit
WHERE unit.shop_code = 4
GROUP BY unit.name";


    $stmt = $pdo->prepare($query);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $result[0]["number of employees"] = $result[0]["COUNT(unit.unit_code)"];
    unset($result[0]["COUNT(unit.unit_code)"]);
    $result[1]["number of employees"] = $result[1]["COUNT(unit.unit_code)"];
    unset($result[1]["COUNT(unit.unit_code)"]);
   echo "<pre>";
   print_r($result);
   echo "<pre>";
}
elseif (isset($_POST["unit2"])) {


    for ($i = 1; $i <= 4; $i++) {

        $query = "SELECT main_shop.city,COUNT(employee.Name) FROM main_shop
INNER JOIN unit
on main_shop.shop_code = unit.shop_code
INNER JOIN employee
ON employee.Unit = unit.unit_code
WHERE main_shop.shop_code = $i
HAVING COUNT(employee.Name) < 4
 ";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        print_r($result);
        echo "<br>";

    }
}

